import router from './router'
import store from './store'

router.beforeEach(async(to, from, next) => {
    try {
        const { roles, routes } = await store.dispatch('user/getInfo')
        console.log('routes', routes);
        const MyAccessRoutes = await store.dispatch('permission/analysisRoutes', routes)
        console.log('MyAccessRoutes', MyAccessRoutes);
        router.addRoute(MyAccessRoutes)
        next({ ...to, replace: true })
    } catch (error) {
        //await store.dispatch('user/resetToken')
        next(`/login?redirect=${to.path}`)
    }
})