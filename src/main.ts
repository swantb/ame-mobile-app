import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { Image as VanImage } from 'vant';
import SvgIcon from './library/components/SvgIcon/index.vue'// svg component

const app = createApp(App)
app.component('svg-icon', SvgIcon)
import './library/assets/icons'
app.use(VanImage)
app.use(store)
app.use(router)
app.mount('#app')
