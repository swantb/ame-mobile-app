import axios from 'axios'
const instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    "Content-Type": "application/json",
    "x-requested-with": "XMLHttpRequest",
  }
})
// 请求拦截器
instance.interceptors.request.use(
  config => {
    let token:string = '555555';
    console.log(token,'yyyyyy')
    if (token) {
      config.headers.Authorization = token
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
instance.interceptors.response.use(
  response => {console.log('d9999');
    return response.data
  },
  error => {
    return Promise.reject(error)
  }
)

export default instance