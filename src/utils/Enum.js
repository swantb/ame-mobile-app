export default class Enum {
  constructor (enumeration) {
    let descMap = {}
    let valueToDescMap = {}
    let options = []

    for (let key in enumeration) {
      if (enumeration.hasOwnProperty(key)) {
        const enumValue = enumeration[key]
        if (Array.isArray(enumValue)) {
          const [
            value,
            description
          ] = enumValue
          this[key] = value
          descMap[key] = description
          valueToDescMap[value] = description
          options.push({
            label: description,
            value: value
          })
        } else {
          this[key] = enumValue
        }
      }
    }

    Object.defineProperty(this, 'descMap', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: descMap
    })
    Object.defineProperty(this, 'valueToDescMap', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: valueToDescMap
    })
    Object.defineProperty(this, 'options', {
      enumerable: false,
      configurable: false,
      writable: false,
      value: options
    })
  }

  descOf (name) {
    if (name && this.descMap[name] !== 'undefined') {
      return this.descMap[name]
    }
    return null
  }

  descOfValue (value) {
    if (value !== 'undefined' && this.valueToDescMap[value] !== 'undefined') {
      return this.valueToDescMap[value]
    }

    return null
  }

  valueToOptions () {
    return this.options
  }
}