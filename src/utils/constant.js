import Enum from './Enum';
// 生活缴费枚举
export const livingPayEnum = new Enum({
  powerRate: [1, '电费'],
  waterRate: [2, '水费'],
  gasBill: [3, '燃气费'],
  propertyFee: [4, '物业费'],
  wideBand: [5, '宽带'],
  fixedPhone: [6, '固话'],
  cableTelevision: [7, '有线电视'],
  oilCardRecharge: [8, '油卡充值'],
  recharge: [9, '话费充值']
})