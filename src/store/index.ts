import { createStore } from 'vuex'
import  user from './modules/user'
import  persistedstate from 'vuex-persistedstate'
export default createStore({
  modules: { 
    user
  },
  plugins:[persistedstate()]
})
