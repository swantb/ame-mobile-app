import request from '@/utils/axios';

export function getInfo(data?:any) {
    return request({
      url: '/userinfo',
      method: 'get',
      params: { ...data }
    })
}